import xml.etree.ElementTree as elementTree

from django.utils.datetime_safe import datetime
# from django.contrib.gis.geos.polygon import Polygon


class XmlParser:

    def __init__(self, root_path: str, resolution: int):
        self.root_path = root_path
        self.resolution = resolution

    def parse_product(self, path: str):
        # './sentinel2_extract/S2B_MSIL2A_20211204T101309_N0301_R022_T33TUL_20211204T130603.SAFE/manifest.safe'

        path_product = self.root_path + path
        tree = elementTree.parse(path_product)
        root = tree.getroot()
        title = str(root[0][0].attrib.get('textInfo'))
        metadata_object1 = root[1][0]

        # period u timestampu
        acquisition_time = datetime.strptime(metadata_object1[0][0][0][0].text, '%Y-%m-%dT%H:%M:%S.%f%z')

        metadata_object2 = root[1][1]
        # orbit number
        orbit = int(metadata_object2[0][0][0][1].text)

        metadata_object4 = root[1][4]

        # transform list of decimal numbers to Polygon
        footprint = metadata_object4[0][0][0][0][0].text.strip(' ').split(' ')
        point_str = 'POLYGON(('
        for i in range(0, footprint.__len__(), 2):
            point_str = point_str + str(float(footprint[i])) + ' ' + str(float(footprint[i + 1])) + ','

        wkt = str(point_str.strip(',') + '))')

        return title, orbit, wkt, acquisition_time

    def parse_tile(self, path: str):
        # './sentinel2_extract/S2B_MSIL2A_20211204T101309_N0301_R022_T33TUL_20211204T130603.SAFE/GRANULE/L2A_T33TUL_A024787_20211204T101631/MTD_TL.xml'

        tree = elementTree.parse(path)
        root = tree.getroot()
        # sensing time, downlink priority, tile id text
        tile_list = [datetime.strptime(root[0][4].text, '%Y-%m-%dT%H:%M:%S.%f%z'), root[0][3].text,
                     root[0][1].text]

        quality_indicators = root[2][0]

        for t in range(0, quality_indicators.__len__()):
            tile_list.append(quality_indicators[t].text)

        # Geometry Info
        # Tile Geocoding
        geo = root[1][0]
        if self.resolution == 20:
            nrows = int(geo[3][0].text)
            ncols = int(geo[3][1].text)
            ulx = int(geo[6][0].text)
            uly = int(geo[6][1].text)
            xdim = int(geo[6][2].text)
            ydim = int(geo[6][3].text)
        else:
            nrows = int(geo[4][0].text)
            ncols = int(geo[4][1].text)
            ulx = int(geo[7][0].text)
            uly = int(geo[7][1].text)
            xdim = int(geo[7][2].text)
            ydim = int(geo[7][3].text)

        return tile_list, [nrows, ncols, ulx, uly, xdim, ydim]
