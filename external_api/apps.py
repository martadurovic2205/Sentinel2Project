from django.apps import AppConfig


class ExternalApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'external_api'

    def ready(self):
        from .data import data_save
        data_save()
