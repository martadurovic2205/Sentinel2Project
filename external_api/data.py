# connect to the API
import threading

from sentinelsat import SentinelAPI
from server.models import Product, Satellite, Tile, Band, MaskType, Geoposition, Resolution, Image, Mask, ApiTimeTable
from .XmlParser import XmlParser
from . import auxiliary_functions


# ingestion_date = '2022-01-06T16:23:47.407Z'


def data_save():
    print("IN FUNCTION")
    field_name = 'ingestion_time'
    obj = ApiTimeTable.objects.last()
    ingestion_date = getattr(obj, field_name)

    #  global ingestion_date
    print(ingestion_date)
    api = SentinelAPI('mdurovic', 'marta2205Durovic', 'https://scihub.copernicus.eu/dhus')

    footprint = 'POLYGON((14.5847 45.7226,13.4038 45.5415,13.5607 44.936,15.5665 43.5146,15.664 43.0022, ' \
                '16.217 42.3413,18.6382 42.3402,18.4623 42.6254,16.5556 44.0833,15.9083 44.9078,18.4599 45.021,' \
                '18.9541 44.733, 19.5345 45.1895,18.9001 46.0192,18.0819 45.8383,16.3968 46.6356,15.5618 46.2256,' \
                '15.0449 45.5402,14.5847 45.7226)) '
#    footprint = 'POLYGON((13.56686 45.54159, 14.57771 45.70566, 15.207515 45.51513, 15.17576 45.74800,' \
#                ' 15.57798 45.92265, 15.51447 46.19785, 16.32421 46.62125, 18.86458 45.94911, 19.50497 45.21346,' \
#                ' 19.06040 44.81124, 16.29246 44.95942, 15.86377 45.13937, 16.28187 44.36667, 17.37212 43.48813,' \
#                ' 18.50999 42.60694, 18.59996 42.38465, 16.23424 42.36878, 14.20724 44.55058, 13.70446 44.83108,' \
#                ' 13.40808 45.52439, 13.56686 45.54159))'
#    footprint = "(POLYGON((13.56 45.54, 14.57 45.70, 15.20 45.51, 15.17 45.74, 15.57 45.92, 15.51 46.19, " \
#                "16.32 46.62, " \
#                "18.86 45.94, 19.50 45.21, 19.06 44.81, 16.29 44.95, 15.86 45.13, 16.28 44.36, 17.37 43.48, " \
#                "18.50 42.60, " \
#                "18.59 42.38, 16.23 42.36, 14.20 44.55, 13.70 44.83, 13.40 45.52, 13.56 45.54)))"

    products = api.query(footprint,
                         date=(ingestion_date, 'NOW'),
                         platformname='Sentinel-2',
                         producttype='S2MSI2A',
                         limit=5)

    df = api.to_dataframe(products)
    if df.__len__() > 0:
        products_df_sorted = df.sort_values('ingestiondate', ascending=False)
        print(products_df_sorted['ingestiondate'])
        list_ing_date = []
        for ing in products_df_sorted['ingestiondate']:
            list_ing_date.append(str(ing).replace(' ', 'T').split('\n')[0] + 'Z')
        # ingestion_date = str(products_df_sorted.head(1)['ingestiondate']).split('  ')[1].removeprefix(' ')
        # .replace(' ', 'T').split('\n')[0] + 'Z'

        ingestion_date = list_ing_date[list_ing_date.__len__() - 1]

        api_time = ApiTimeTable(ingestion_time=ingestion_date)
        api_time.save()
        print("Ima produkata")
        directory = 'M:/Sentinel2Project/data_access/sentinel2_zip'
        dict_ex = 'M:/Sentinel2Project/data_access/sentinel2_extract'
        list_search = [api.download(item, directory_path=directory) for item in products_df_sorted["uuid"]]
        [auxiliary_functions.function_unzip(item, directory, dict_ex) for item in products_df_sorted['title']]

        for i in range(0, list_search.__len__()):
            list_s = [dict_ex + '/' + list_search[i]['title'] + '.SAFE']

            for p in list_s:
                resolution = auxiliary_functions.check_resolution(p)
                xml_parser = XmlParser(p, resolution)
                list_of_auxiliary_files = ['GENERAL_QUALITY.xml', 'GEOMETRIC_QUALITY.xml', 'FORMAT_CORRECTNESS.xml',
                                           'RADIOMETRIC_QUALITY.xml', 'SENSOR_QUALITY.xml']
                xml_list = []
                for j in list_of_auxiliary_files:
                    xml_list.append(auxiliary_functions.saving_files_in_database(auxiliary_functions
                                                                                 .find(j, p + '/DATASTRIP'), 'xml'))

                product_list = xml_parser.parse_product('/manifest.safe')
                if list_search[i]['title'].startswith('S2A'):
                    id_satellite = 1
                else:
                    id_satellite = 2

                product = Product(title=product_list[0], orbit=product_list[1], footprint=product_list[2],
                                  acquisition_time=product_list[3], ingestion_date=list_ing_date[i],
                                  id_satellite=Satellite.objects.get(id=id_satellite),
                                  general_quality=xml_list[0], geometric_quality=xml_list[1],
                                  format_correctness=xml_list[2], radiometric_quality=xml_list[3],
                                  sensor_quality=xml_list[4])
                product.save()

                tile_list, geo_list = xml_parser.parse_tile(auxiliary_functions.find('MTD_TL.xml', p))
                list_of_auxiliary_files_2 = ['GENERAL_QUALITY.xml', 'GEOMETRIC_QUALITY.xml', 'FORMAT_CORRECTNESS.xml',
                                             'SENSOR_QUALITY.xml']
                xml_list_2 = []
                for k in list_of_auxiliary_files_2:
                    xml_list_2.append(auxiliary_functions.saving_files_in_database(auxiliary_functions
                                                                                   .find(k, p + '/GRANULE'), 'xml'))

                tile = Tile(sensing_time=tile_list[0], downlink_priority=tile_list[1], tile_id_text=tile_list[2],
                            cloudy_pixel_percentage=tile_list[3], cloudy_pixel_over_land_percentage=float(tile_list[4]),
                            degraded_msi_data_percentage=float(tile_list[5]),
                            nodata_pixel_percentage=float(tile_list[6]),
                            saturated_defective_pixel_percentage=float(tile_list[7]),
                            dark_features_percentage=float(tile_list[8]), cloud_shadow_percentage=float(tile_list[9]),
                            vegetation_percentage=float(tile_list[10]), not_vegetated_percentage=float(tile_list[11]),
                            water_percentage=float(tile_list[12]), unclassified_percentage=float(tile_list[13]),
                            medium_proba_clouds_percentage=float(tile_list[14]),
                            high_proba_clouds_percentage=float(tile_list[15]),
                            thin_cirrus_percentage=float(tile_list[16]), snow_ice_percentage=float(tile_list[17]),
                            radiative_transfer_accuracy=float(tile_list[18]),
                            water_vapour_retrieval_accuracy=float(tile_list[19]),
                            aot_retrieval_accuracy=float(tile_list[20]), aot_retrieval_method=str(tile_list[21]),
                            granule_mean_aot=float(tile_list[22]), granule_mean_wv=float(tile_list[23]),
                            ozone_source=str(tile_list[24]), ozone_value=float(tile_list[25]),
                            general_quality=xml_list_2[0],
                            geometric_quality=xml_list_2[1], format_correctness=xml_list_2[2],
                            sensor_quality=xml_list_2[3], id_product=product)
                tile.save()

                geoposition = Geoposition(id_tile=tile,
                                          id_resolution=Resolution.objects.get(name=str(resolution) + 'm'),
                                          nrows=geo_list[0],
                                          ncols=geo_list[1], ulx=geo_list[2], uly=geo_list[3],
                                          xdim=geo_list[4], ydim=geo_list[5])
                geoposition.save()

                path_for_image = auxiliary_functions.find('R' + str(resolution) + 'm', p + '/GRANULE')

                image_read = auxiliary_functions.saving_files_in_database(
                    auxiliary_functions.find_files_with_certain_names(path_for_image, '8A'), 'image')

                image = Image(id_tile=tile, id_band=Band.objects.get(number='8A'),
                              id_resolution=Resolution.objects.get(name=str(resolution) + 'm'),
                              image=image_read)
                image.save()

                image_read_2 = auxiliary_functions.saving_files_in_database(
                    auxiliary_functions.find_files_with_certain_names(path_for_image, '12'), 'image')

                image_2 = Image(id_tile=tile, id_band=Band.objects.get(number='12'),
                                id_resolution=Resolution.objects.get(name=str(resolution) + 'm'),
                                image=image_read_2)
                image_2.save()

                path_for_masks = auxiliary_functions.find('QI_DATA', p + '/GRANULE')

                mask_types = MaskType.objects.all().values_list('type', flat=True)
                band_number = ['00', '8A', '12']
                for b in band_number:
                    for m in mask_types:
                        mask_path = auxiliary_functions.find_files_with_certain_names(path_for_masks, b, m)
                        if mask_path != '':
                            mask = Mask(id_tile=tile, id_band=Band.objects.get(number=b),
                                        id_mask_type=MaskType.objects.get(type=m),
                                        mask=auxiliary_functions.saving_files_in_database(mask_path, 'image'))
                            mask.save()
    else:
        print("ESA api nema novih produkata")

    threading.Timer(43200, data_save).start()
