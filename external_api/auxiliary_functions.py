import os
from zipfile import ZipFile


def function_unzip(file_name, directory, destination):
    # opening the zip file in READ mode
    with ZipFile(directory + '/' + file_name + '.zip', 'r') as zipfile:
        zipfile.extractall(destination)


# function for reading files to store into database
def saving_files_in_database(file, format_type: str):
    if format_type == 'image':
        with open(file, 'rb') as f:
            temp_file = f.read()
    elif format_type == 'xml':
        with open(file, 'r') as f:
            temp_file = f.read()
    return temp_file


# function for finding xml files to store into database or findig directories that I need
def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)
        elif name in dirs:
            return os.path.join(root, name)


# function for checking if there is resolution 20 for product
def check_resolution(path):
    for root, dirs, files in os.walk(path):
        if dirs.__contains__('R20m'):
            if dirs.__len__() == 0:
                resolution = 60
            else:
                resolution = 20
            return resolution


def find_files_with_certain_names(path, name, *args):
    image = ''
    if args.__len__() == 0:
        for file in os.listdir(path):
            if file.__contains__(name):
                image = os.path.join(path, file).replace('\\', '/')
    elif args.__len__() == 1:
        for file in os.listdir(path):
            if file.__contains__(name) and file.__contains__(args[0]):
                image = os.path.join(path, file).replace('\\', '/')
    else:
        image = ''
    return image
