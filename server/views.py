# from io import BytesIO
# from PIL import Image

from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from .models import Product, Tile
from django.http import HttpResponse
import numpy as np
from django.core.serializers import serialize

# from .models import Image as Image_model
# from matplotlib import pyplot as plt
# from geo.Geoserver import Geoserver
from .serializers import ProductSerializer, TileSerializer, ProductGeoSerializer


# geoserver = Geoserver('http://127.0.0.1:8080/geoserver', username='admin', password='gis2022')

# images = Image_model.objects.all().values_list('image', flat=True)
# for i in images:
#    image = BytesIO(i)
#    final_image = Image.open(image)
#    plt.imshow(final_image)
#    plt.show()

# masks = Mask.objects.all().values_list('mask', flat=True)
# for m in masks:
#    mask = BytesIO(m)
#    final_mask = Image.open(mask)


@api_view(['GET'])
def product_list(request):
    products = Product.objects.all()
    title = request.GET.get('title', None)
    if title is not None:
        products = products.filter(title=title)
    product_serializer = ProductSerializer(products, many=True)
    return HttpResponse(product_serializer.data, content_type='application/json')


@api_view(['GET'])
def search_product_list(request):
    from_date = request.query_params['from']
    to_date = request.query_params['to']
    search_result = Product.objects.filter(ingestion_date__range=(from_date, to_date))
    product_serializer = ProductSerializer(search_result, many=True)
    return JsonResponse(product_serializer.data, safe=False)


def flip_geojson_coordinates(geo):
    if isinstance(geo, dict):
        for k, v in geo.items():
            if k == "coordinates":
                z = np.asarray(geo[k])
                f = z.flatten()
                geo[k] = np.dstack((f[1::2], f[::2])).reshape(z.shape).tolist()
            else:
                flip_geojson_coordinates(v)
    elif isinstance(geo, list):
        for k in geo:
            flip_geojson_coordinates(k)


@api_view(['GET'])
def product_detail(request, pk):
    # find product by pk (id)
    try:
        product = Product.objects.get(id=pk)
    except ObjectDoesNotExist:
        return JsonResponse({'message': 'The product does not exist'}, status=status.HTTP_404_NOT_FOUND)
    product_serializer = ProductGeoSerializer(product)
    flip_geojson_coordinates(product_serializer.data)
    return JsonResponse(product_serializer.data, safe=False)


@api_view(['GET'])
def tile_list(request):
    tiles = Tile.objects.all()
    tile_id_text = request.GET.get('tile_id_text', None)
    if tile_id_text is not None:
        tiles = tiles.filter(tile_id_text=tile_id_text)
    tile_serializer = TileSerializer(tiles, many=True)
    return JsonResponse(tile_serializer.data, safe=False)


@api_view(['GET'])
def tile_detail(request, pk):
    # find tile by pk (id)
    try:
        tile = Tile.objects.get(id=pk)
    except ObjectDoesNotExist:
        return JsonResponse({'message': 'The tile does not exist'}, status=status.HTTP_404_NOT_FOUND)
    tile_serializer = TileSerializer(tile)
    return JsonResponse(tile_serializer.data, safe=False)


