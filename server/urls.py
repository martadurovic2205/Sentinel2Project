from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^products$', views.product_list),
    re_path(r'^products/(?P<pk>[0-9]+)$', views.product_detail),
    re_path(r'^tiles$', views.tile_list),
    re_path(r'^tiles/(?P<pk>[0-9]+)$', views.tile_detail),
    re_path(r'^search$', views.search_product_list)
]