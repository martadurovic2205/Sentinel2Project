# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from django.contrib.gis.db import models


class Band(models.Model):
    objects = models.Manager()
    id = models.IntegerField(primary_key=True, db_column='band_id')
    number = models.CharField(max_length=2)
    spatial_resolution = models.IntegerField()
    id_resolution = models.ForeignKey('Resolution', models.DO_NOTHING, db_column='resolution_id_resolution')

    class Meta:
        managed = False
        db_table = 'band'


class Geoposition(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='geoposition_id')
    id_tile = models.OneToOneField('Tile', models.DO_NOTHING, db_column='tile_id_tile')
    id_resolution = models.ForeignKey('Resolution', models.DO_NOTHING, db_column='resolution_id_resolution')
    nrows = models.IntegerField(blank=True, null=True)
    ncols = models.IntegerField(blank=True, null=True)
    ulx = models.IntegerField(blank=True, null=True)
    uly = models.IntegerField(blank=True, null=True)
    xdim = models.IntegerField(blank=True, null=True)
    ydim = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'geoposition'
        unique_together = (('id_tile', 'id_resolution'),)


class Image(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='image_id')
    id_tile = models.ForeignKey('Tile', models.DO_NOTHING, db_column='tile_id_tile')
    id_band = models.ForeignKey(Band, models.DO_NOTHING, db_column='band_id_band', blank=True, null=True)
    id_resolution = models.ForeignKey('Resolution', models.DO_NOTHING, db_column='resolution_id_resolution')
    image = models.BinaryField()

    class Meta:
        managed = False
        db_table = 'image'
        unique_together = (('id_tile', 'id_resolution', 'id_band'),)


class Mask(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='mask_id')
    id_tile = models.ForeignKey('Tile', models.DO_NOTHING, db_column='tile_id_tile')
    id_band = models.ForeignKey(Band, models.DO_NOTHING, db_column='band_id_band')
    id_mask_type = models.ForeignKey('MaskType', models.DO_NOTHING, db_column='mask_type_id_mask_type')
    mask = models.BinaryField()

    class Meta:
        managed = False
        db_table = 'mask'
        unique_together = (('id_tile', 'id_mask_type', 'id_band'),)


class MaskType(models.Model):
    objects = models.Manager()
    id = models.IntegerField(primary_key=True, db_column='mask_type_id')
    type = models.TextField()

    class Meta:
        managed = False
        db_table = 'mask_type'


class Product(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='product_id')
    title = models.TextField()
    orbit = models.IntegerField()
    footprint = models.PolygonField(srid=3857)
    acquisition_time = models.DateTimeField()
    ingestion_date = models.DateTimeField()
    general_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    geometric_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    format_correctness = models.TextField(blank=True, null=True)  # This field type is a guess.
    radiometric_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    sensor_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_satellite = models.ForeignKey('Satellite', models.DO_NOTHING, db_column='satellite_id_satellite')

    class Meta:
        managed = False
        db_table = 'product'


class Resolution(models.Model):
    objects = models.Manager()
    id = models.IntegerField(primary_key=True, db_column='resolution_id')
    name = models.TextField()

    class Meta:
        managed = False
        db_table = 'resolution'


class Satellite(models.Model):
    objects = models.Manager()
    id = models.IntegerField(primary_key=True, db_column='satellite_id')
    name = models.TextField()

    class Meta:
        managed = False
        db_table = 'satellite'


class Tile(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='tile_id')
    sensing_time = models.DateTimeField()
    downlink_priority = models.TextField()
    tile_id_text = models.TextField()
    cloudy_pixel_percentage = models.FloatField(blank=True, null=True)
    cloudy_pixel_over_land_percentage = models.FloatField(blank=True, null=True)
    degraded_msi_data_percentage = models.FloatField(blank=True, null=True)
    nodata_pixel_percentage = models.FloatField(blank=True, null=True)
    saturated_defective_pixel_percentage = models.FloatField(blank=True, null=True)
    dark_features_percentage = models.FloatField(blank=True, null=True)
    cloud_shadow_percentage = models.FloatField(blank=True, null=True)
    vegetation_percentage = models.FloatField(blank=True, null=True)
    not_vegetated_percentage = models.FloatField(blank=True, null=True)
    water_percentage = models.FloatField(blank=True, null=True)
    unclassified_percentage = models.FloatField(blank=True, null=True)
    medium_proba_clouds_percentage = models.FloatField(blank=True, null=True)
    high_proba_clouds_percentage = models.FloatField(blank=True, null=True)
    thin_cirrus_percentage = models.FloatField(blank=True, null=True)
    snow_ice_percentage = models.FloatField(blank=True, null=True)
    radiative_transfer_accuracy = models.FloatField(blank=True, null=True)
    water_vapour_retrieval_accuracy = models.FloatField(blank=True, null=True)
    aot_retrieval_accuracy = models.FloatField(blank=True, null=True)
    aot_retrieval_method = models.TextField(blank=True, null=True)
    granule_mean_aot = models.FloatField(blank=True, null=True)
    granule_mean_wv = models.FloatField(blank=True, null=True)
    ozone_source = models.TextField(blank=True, null=True)
    ozone_value = models.FloatField(blank=True, null=True)
    general_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    geometric_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    format_correctness = models.TextField(blank=True, null=True)  # This field type is a guess.
    sensor_quality = models.TextField(blank=True, null=True)  # This field type is a guess.
    id_product = models.ForeignKey(Product, models.DO_NOTHING, db_column='product_id_product')

    class Meta:
        managed = False
        db_table = 'tile'


class ApiTimeTable(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True, db_column='time_id')
    ingestion_time = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'api_time_table'
