from rest_framework import serializers
from .models import Product, Tile
from rest_framework_gis import serializers as gis_serializers


class ProductGeoSerializer(gis_serializers.GeoModelSerializer):
    acquisition_time = serializers.SerializerMethodField()
    ingestion_date = serializers.SerializerMethodField()

    class Meta:
        model = Product
        geo_field = 'footprint'
        id_field = False
        fields = ('id', 'title', 'orbit', 'footprint', 'acquisition_time', 'ingestion_date')


    def get_acquisition_time(self, obj):
        return obj.acquisition_time.strftime("%m-%d-%Y, %H:%M:%S")

    def get_ingestion_date(self, obj):
        return obj.ingestion_date.strftime("%m-%d-%Y, %H:%M:%S")


class ProductSerializer(serializers.ModelSerializer):
    acquisition_time = serializers.SerializerMethodField()
    ingestion_date = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = ('id', 'title', 'orbit', 'acquisition_time', 'ingestion_date')

    def get_acquisition_time(self, obj):
        return obj.acquisition_time.strftime("%m-%d-%Y, %H:%M:%S")

    def get_ingestion_date(self, obj):
        return obj.ingestion_date.strftime("%m-%d-%Y, %H:%M:%S")


class TileSerializer(serializers.ModelSerializer):
    sensing_time = serializers.SerializerMethodField()

    class Meta:
        model = Tile
        fields = (
            'id', 'sensing_time', 'downlink_priority', 'tile_id_text', 'cloudy_pixel_percentage',
            'degraded_msi_data_percentage',
            'nodata_pixel_percentage', 'saturated_defective_pixel_percentage', 'dark_features_percentage',
            'cloud_shadow_percentage', 'vegetation_percentage', 'not_vegetated_percentage', 'water_percentage',
            'unclassified_percentage', 'medium_proba_clouds_percentage', 'high_proba_clouds_percentage',
            'thin_cirrus_percentage', 'snow_ice_percentage', 'radiative_transfer_accuracy',
            'water_vapour_retrieval_accuracy', 'aot_retrieval_accuracy', 'aot_retrieval_method', 'granule_mean_aot',
            'granule_mean_wv', 'ozone_source', 'ozone_value')

    def get_sensing_time(self, obj):
        return obj.sensing_time.strftime("%m-%d-%Y, %H:%M:%S")
