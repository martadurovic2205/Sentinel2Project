-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler version: 0.9.4
-- PostgreSQL version: 13.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- Database creation must be performed outside a multi lined SQL file. 
-- These commands were put in this file only as a convenience.
-- 
-- -- object: postgres | type: DATABASE --
-- -- DROP DATABASE IF EXISTS postgres;
-- CREATE DATABASE postgres
-- 	OWNER = postgres;
-- -- ddl-end --
-- 

-- object: sen2 | type: SCHEMA --
-- DROP SCHEMA IF EXISTS sen2 CASCADE;
CREATE SCHEMA sen2;
-- ddl-end --

SET search_path TO pg_catalog,public,sen2;
-- ddl-end --

-- object: sen2.satellite | type: TABLE --
-- DROP TABLE IF EXISTS sen2.satellite CASCADE;
CREATE TABLE sen2.satellite (
	satellite_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	name text NOT NULL,
	CONSTRAINT satellite_pk PRIMARY KEY (satellite_id)
);
-- ddl-end --
ALTER TABLE sen2.satellite OWNER TO postgres;
-- ddl-end --

-- object: postgis | type: EXTENSION --
-- DROP EXTENSION IF EXISTS postgis CASCADE;
UPDATE pg_extension
SET extrelocatable = TRUE
    WHERE extname = 'postgis';

ALTER EXTENSION postgis SET SCHEMA sen2;
-- ddl-end --
COMMENT ON EXTENSION postgis IS E'PostGIS geometry, geography, and raster spatial types and functions';
-- ddl-end --

-- object: sen2.tile | type: TABLE --
-- DROP TABLE IF EXISTS sen2.tile CASCADE;
CREATE TABLE sen2.tile (
	tile_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	sensing_time timestamp NOT NULL,
	downlink_priority text NOT NULL,
	tile_id_text text NOT NULL,
	cloudy_pixel_percentage float4,
	cloudy_pixel_over_land_percentage float4,
	degraded_msi_data_percentage float4,
	nodata_pixel_percentage float4,
	saturated_defective_pixel_percentage float4,
	dark_features_percentage float4,
	cloud_shadow_percentage float4,
	vegetation_percentage float4,
	not_vegetated_percentage float4,
	water_percentage float4,
	unclassified_percentage float4,
	medium_proba_clouds_percentage float4,
	high_proba_clouds_percentage float4,
	thin_cirrus_percentage float4,
	snow_ice_percentage float4,
	radiative_transfer_accuracy float4,
	water_vapour_retrieval_accuracy float4,
	aot_retrieval_accuracy float4,
	aot_retrieval_method text,
	granule_mean_aot float4,
	granule_mean_wv float4,
	ozone_source text,
	ozone_value float4,
	general_quality xml,
	geometric_quality xml,
	format_correctness xml,
	sensor_quality xml,
	product_id_product integer NOT NULL,
	CONSTRAINT tile_pk PRIMARY KEY (tile_id)
);
-- ddl-end --
ALTER TABLE sen2.tile OWNER TO postgres;
-- ddl-end --

-- object: sen2.geoposition | type: TABLE --
-- DROP TABLE IF EXISTS sen2.geoposition CASCADE;
CREATE TABLE sen2.geoposition (
	geoposition_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	nrows integer,
	ncols integer,
	ulx integer,
	uly integer,
	xdim integer,
	ydim integer,
	resolution_id_resolution integer NOT NULL,
	tile_id_tile integer NOT NULL,
	CONSTRAINT geoposition_pk PRIMARY KEY (geoposition_id)
);
-- ddl-end --
ALTER TABLE sen2.geoposition OWNER TO postgres;
-- ddl-end --

-- object: sen2.resolution | type: TABLE --
-- DROP TABLE IF EXISTS sen2.resolution CASCADE;
CREATE TABLE sen2.resolution (
	resolution_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	name text NOT NULL,
	CONSTRAINT resolution_pk PRIMARY KEY (resolution_id)
);
-- ddl-end --
ALTER TABLE sen2.resolution OWNER TO postgres;
-- ddl-end --

-- object: sen2.product | type: TABLE --
-- DROP TABLE IF EXISTS sen2.product CASCADE;
CREATE TABLE sen2.product (
	product_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	title text NOT NULL,
	orbit integer NOT NULL,
	footprint geometry(POLYGON, 4326) NOT NULL,
	acquisition_time timestamp NOT NULL,
	ingestion_date timestamp NOT NULL,
	general_quality xml,
	geometric_quality xml,
	format_correctness xml,
	radiometric_quality xml,
	sensor_quality xml,
	satellite_id_satellite integer NOT NULL,
	CONSTRAINT product_pk PRIMARY KEY (product_id)
);
-- ddl-end --
ALTER TABLE sen2.product OWNER TO postgres;
-- ddl-end --

-- object: sen2.band | type: TABLE --
-- DROP TABLE IF EXISTS sen2.band CASCADE;
CREATE TABLE sen2.band (
	band_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	number varchar(2) NOT NULL,
	spatial_resolution integer,
	resolution_id_resolution integer,
	CONSTRAINT band_pk PRIMARY KEY (band_id)
);
-- ddl-end --
ALTER TABLE sen2.band OWNER TO postgres;
-- ddl-end --

-- object: sen2.image | type: TABLE --
-- DROP TABLE IF EXISTS sen2.image CASCADE;
CREATE TABLE sen2.image (
	image_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	image bytea NOT NULL,
	tile_id_tile integer NOT NULL,
	resolution_id_resolution integer NOT NULL,
	band_id_band integer NOT NULL,
	CONSTRAINT image_pk PRIMARY KEY (image_id)
);
-- ddl-end --
ALTER TABLE sen2.image OWNER TO postgres;
-- ddl-end --

-- object: sen2.mask | type: TABLE --
-- DROP TABLE IF EXISTS sen2.mask CASCADE;
CREATE TABLE sen2.mask (
	mask_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	mask bytea NOT NULL,
	tile_id_tile integer NOT NULL,
	band_id_band integer NOT NULL,
	mask_type_id_mask_type integer NOT NULL,
	CONSTRAINT mask_pk PRIMARY KEY (mask_id)
);
-- ddl-end --
ALTER TABLE sen2.mask OWNER TO postgres;
-- ddl-end --

-- object: tile_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.geoposition DROP CONSTRAINT IF EXISTS tile_fk CASCADE;
ALTER TABLE sen2.geoposition ADD CONSTRAINT tile_fk FOREIGN KEY (tile_id_tile)
REFERENCES sen2.tile (tile_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: resolution_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.geoposition DROP CONSTRAINT IF EXISTS resolution_fk CASCADE;
ALTER TABLE sen2.geoposition ADD CONSTRAINT resolution_fk FOREIGN KEY (resolution_id_resolution)
REFERENCES sen2.resolution (resolution_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: resolution_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.image DROP CONSTRAINT IF EXISTS resolution_fk CASCADE;
ALTER TABLE sen2.image ADD CONSTRAINT resolution_fk FOREIGN KEY (resolution_id_resolution)
REFERENCES sen2.resolution (resolution_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: tile_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.image DROP CONSTRAINT IF EXISTS tile_fk CASCADE;
ALTER TABLE sen2.image ADD CONSTRAINT tile_fk FOREIGN KEY (tile_id_tile)
REFERENCES sen2.tile (tile_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: band_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.image DROP CONSTRAINT IF EXISTS band_fk CASCADE;
ALTER TABLE sen2.image ADD CONSTRAINT band_fk FOREIGN KEY (band_id_band)
REFERENCES sen2.band (band_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: tile_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.mask DROP CONSTRAINT IF EXISTS tile_fk CASCADE;
ALTER TABLE sen2.mask ADD CONSTRAINT tile_fk FOREIGN KEY (tile_id_tile)
REFERENCES sen2.tile (tile_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: band_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.mask DROP CONSTRAINT IF EXISTS band_fk CASCADE;
ALTER TABLE sen2.mask ADD CONSTRAINT band_fk FOREIGN KEY (band_id_band)
REFERENCES sen2.band (band_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: sen2.mask_type | type: TABLE --
-- DROP TABLE IF EXISTS sen2.mask_type CASCADE;
CREATE TABLE sen2.mask_type (
	mask_type_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	type text NOT NULL,
	CONSTRAINT mask_type_pk PRIMARY KEY (mask_type_id)
);
-- ddl-end --
ALTER TABLE sen2.mask_type OWNER TO postgres;
-- ddl-end --

-- object: mask_type_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.mask DROP CONSTRAINT IF EXISTS mask_type_fk CASCADE;
ALTER TABLE sen2.mask ADD CONSTRAINT mask_type_fk FOREIGN KEY (mask_type_id_mask_type)
REFERENCES sen2.mask_type (mask_type_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: product_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.tile DROP CONSTRAINT IF EXISTS product_fk CASCADE;
ALTER TABLE sen2.tile ADD CONSTRAINT product_fk FOREIGN KEY (product_id_product)
REFERENCES sen2.product (product_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: satellite_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.product DROP CONSTRAINT IF EXISTS satellite_fk CASCADE;
ALTER TABLE sen2.product ADD CONSTRAINT satellite_fk FOREIGN KEY (satellite_id_satellite)
REFERENCES sen2.satellite (satellite_id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: mask_u | type: CONSTRAINT --
-- ALTER TABLE sen2.mask DROP CONSTRAINT IF EXISTS mask_u CASCADE;
ALTER TABLE sen2.mask ADD CONSTRAINT mask_u UNIQUE (tile_id_tile,mask_type_id_mask_type,band_id_band);
-- ddl-end --

-- object: image_u | type: CONSTRAINT --
-- ALTER TABLE sen2.image DROP CONSTRAINT IF EXISTS image_u CASCADE;
ALTER TABLE sen2.image ADD CONSTRAINT image_u UNIQUE (tile_id_tile,resolution_id_resolution,band_id_band);
-- ddl-end --

-- object: resolution_fk | type: CONSTRAINT --
-- ALTER TABLE sen2.band DROP CONSTRAINT IF EXISTS resolution_fk CASCADE;
ALTER TABLE sen2.band ADD CONSTRAINT resolution_fk FOREIGN KEY (resolution_id_resolution)
REFERENCES sen2.resolution (resolution_id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: geoposition_u | type: CONSTRAINT --
-- ALTER TABLE sen2.geoposition DROP CONSTRAINT IF EXISTS geoposition_u CASCADE;
ALTER TABLE sen2.geoposition ADD CONSTRAINT geoposition_u UNIQUE (resolution_id_resolution,tile_id_tile);
-- ddl-end --

-- object: sen2.api_time_table | type: TABLE --
-- DROP TABLE IF EXISTS sen2.api_time_table CASCADE;
CREATE TABLE sen2.api_time_table (
	time_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	ingestion_time timestamp NOT NULL,
	CONSTRAINT api_time_table_pk PRIMARY KEY (time_id)
);
-- ddl-end --
ALTER TABLE sen2.api_time_table OWNER TO postgres;
-- ddl-end --


