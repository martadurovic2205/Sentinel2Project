INSERT INTO sen2.api_time_table(ingestion_time) VALUES('2022-01-06T00:00:00.000Z');

INSERT INTO sen2.satellite(name) VALUES('Sentinel-2A');
INSERT INTO sen2.satellite(name) VALUES('Sentinel-2B');

INSERT INTO sen2.resolution(name) VALUES('10m');
INSERT INTO sen2.resolution(name) VALUES('20m');
INSERT INTO sen2.resolution(name) VALUES('60m');

INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES ('00', NULL, NULL);
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('1', 443, (SELECT resolution_id FROM sen2.resolution WHERE name='60m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('2', 490, (SELECT resolution_id FROM sen2.resolution WHERE name='10m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('3', 560, (SELECT resolution_id FROM sen2.resolution WHERE name='10m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('4', 665, (SELECT resolution_id FROM sen2.resolution WHERE name='10m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('5', 705, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('6', 740, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('7', 783, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('8', 842, (SELECT resolution_id FROM sen2.resolution WHERE name='10m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('8A', 865, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('9', 940, (SELECT resolution_id FROM sen2.resolution WHERE name='60m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('10', 1375, (SELECT resolution_id FROM sen2.resolution WHERE name='60m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('11', 1610, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));
INSERT INTO sen2.band(number, spatial_resolution, resolution_id_resolution) VALUES('12', 2190, (SELECT resolution_id FROM sen2.resolution WHERE name='20m'));

INSERT INTO sen2.mask_type(type) VALUES('CLASSI');
INSERT INTO sen2.mask_type(type) VALUES('DETFOO');
INSERT INTO sen2.mask_type(type) VALUES('QUALIT');